<?php

namespace app\api\controller;

use think\Controller;

class Duiba extends Controller
{
    /**
     * 扣除积分
     */
    public function score_dec()
    {
        $input = input();
        $db_config = get_db_config(true);
        $duiba = new \ddj\Duiba();
        $verify = $duiba->signVerify($db_config['app_duiba']['appsecret'],$input);
        if(!$verify){
            $msg = [
                'status' => 'fail',
                'errorMessage' => '签名错误',
                'credits' => 0,
            ];
            return $msg;
        }else {
            $uid = input('uid');
            $uinfo = model('User')->get($uid);
            if (!$uinfo){
                $msg = [
                    'status' => 'fail',
                    'errorMessage' => '用户信息不存在',
                    'credits' => 0,
                ];
                return $msg;
            }else {
                $credits = input('credits');
                if ($uinfo['score']<=$credits){
                    $msg = [
                        'status' => 'fail',
                        'errorMessage' => '积分不足',
                        'credits' => $uinfo['score'],
                    ];
                    return $msg;
                }
                $re = add_score($input['type'], $uid,['score'=>-$credits,'remark'=>$input['description'],'attach'=>$input['orderNum']]);
                if ($re){
                    $logs = model('DuibaLogs');
                    $res = $logs->allowField(true)->save($input);
                    $msg = [
                        'status' => 'ok',
                        'errorMessage' => '兑换成功',
                        'credits' => $uinfo['score']-$credits,
                        'bizId' => $logs->id,
                    ];
                    return $msg;
                }else {
                    $msg = [
                        'status' => 'fail',
                        'errorMessage' => '兑换失败',
                        'credits' => $uinfo['score'],
                    ];
                    return $msg;
                }
            }
        }
        
    }
    /**
     * 增加积分
     */
    public function score_inc()
    {
        $input = input();
        $db_config = get_db_config(true);
        $duiba = new \ddj\Duiba();
        $verify = $duiba->signVerify($db_config['app_duiba']['appsecret'],$input);
        if(!$verify){
            $msg = [
                'status' => 'fail',
                'errorMessage' => '签名错误',
                'credits' => 0,
            ];
            return $msg;
        }else {
            $uid = input('uid');
            $uinfo = model('User')->get($uid);
            if (!$uinfo){
                $msg = [
                    'status' => 'fail',
                    'errorMessage' => '用户信息不存在',
                    'credits' => 0,
                ];
                return $msg;
            }else {
                $credits = input('credits');
                if (db('score_logs')->where(['score'=>$credits,'attach'=>$input['orderNum']])->find()){
                	$msg = [
                		'status' => 'fail',
                		'errorMessage' => '请勿重复操作',
                		'credits' => $uinfo['score'],
                	];
                	return $msg;
                }
                $re = add_score($input['type'], $uid,['score'=>$credits,'remark'=>$input['description'],'attach'=>$input['orderNum']]);
                if ($re){
                    $msg = [
                        'status' => 'ok',
                        'errorMessage' => '增加成功',
                        'credits' => $uinfo['score']+$credits,
                        'bizId' => $input['orderNum'],
                    ];
                    return $msg;
                }else {
                    $msg = [
                        'status' => 'fail',
                        'errorMessage' => '兑换失败',
                        'credits' => $uinfo['score'],
                    ];
                    return $msg;
                }
            }
        }
    }
    /**
     * 通知地址
     */
    public function notify()
    {
        echo 'ok';
    }
}
