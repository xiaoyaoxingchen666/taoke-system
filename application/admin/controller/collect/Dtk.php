<?php

namespace app\admin\controller\collect;

use think\Controller;

class Dtk extends Collect
{
    protected $appkey = '9264n5yrrv';
    /**
     * TOP100人气榜API接口
     */
    public function top100()
    {
        if (!IS_AJAX){
            return '';
        }
        $url = "http://api.dataoke.com/index.php?r=Port/index&type=top100&appkey={$this->appkey}&v=2";
        if(self::_collect($url)){
            $this->success('采集成功');
        }else {
            $this->error('采集失败');
        }
        
    }
    /**
     * 全站领券采集
     */
    public function qzlq()
    {
        $url = "http://api.dataoke.com/index.php?r=Port/index&type=total&appkey={$this->appkey}&v=2";
        self::page_collect($url);
    }
    /**
     * 实时跑量采集
     */
    public function sspl()
    {
        $url = "http://api.dataoke.com/index.php?r=Port/index&type=paoliang&appkey={$this->appkey}&v=2";
        self::page_collect($url);
    }
    /**
     * 分页采集
     */
    private function page_collect($url){
        if (IS_AJAX){
            $page_start = intval(input('page_start'));
            $page_end = intval(input('page_end'));
            if ($page_start==0 || $page_end==0) $this->error('页码不正确~');
            if ($page_start > $page_end) $this->error('页码不正确');
            //开始采集
            $url = $url.'&page='.$page_start;
            self::_collect($url);
            if ($page_start==$page_end) {//终止
                $this->success('采集完成！');
            }
            $this->result(['page_start'=>$page_start+1,'page_end'=>$page_end],2,'正在采集第：'.($page_start+1).'页，请不要关闭本页面~');
        }
    }
    
    /**
     * 采集函数
     */
    private function _collect($url)
    {
        $re = http_get($url);
        $re = json_decode($re,true);
        $result = $re['result'];
        $data = [];
        $item_ids = [];
        foreach ($result as $k=>$v){
            $temp = [
                'cid' => $v['Cid'],
                'title' => $v['Title'],
                'short_title' => $v['D_title'],
                'introduce' => $v['Introduce'],
                'item_id' => $v['GoodsID'],
                'pic' => $v['Pic'],
                'price' => $v['Org_Price'],
                'price_after_quan' => $v['Price'],
                'sales' => $v['Sales_num'],
                'rate' => $v['Commission'],
                'quan_id' => $v['Quan_id'],
                'quan_price' => $v['Quan_price'],
                'quan_start_time' => date('Y-m-d H:i:s'),
                'quan_end_time' => $v['Quan_time'],
                'quan_total' => $v['Quan_surplus']+$v['Quan_receive'],
                'quan_shengyu' => $v['Quan_surplus'],
                'quan_link' => $v['Quan_link'],
                'quan_condition' => $v['Quan_condition'],
                'is_tmall' => $v['IsTmall'],
                'seller_id' => $v['SellerID'],
                'from' => 'dtk',
            ];
            $item_ids[] = $v['GoodsID'];
            $data[] = $temp;
        }
        //dump($data);die;
        //先删除，后添加
        if ($this->model->where('item_id','in',$item_ids)->delete()!==false){
            if ($this->model->saveAll($data)){
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
