<?php
// +----------------------------------------------------------------------
// | llpt system
// +----------------------------------------------------------------------
// | Copyright (c) 2017 llpt All rights reserved.
// +----------------------------------------------------------------------
// | Author: Gooe <zqscjj@163.com> QQ:81009953
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\common\controller\Admin;
use think\cache\driver\Redis;
use think\Db;
use think\facade\Cache;
/**
* 后台设置控制器
*/
class Push extends Admin
{
    private $jpush_key = 'JPUSH-SEND-LIST';

    public function initialize()
    {
        parent::initialize();
    }
    /**
     * 基础设置
     */
    public function index()
    {
        return $this->fetch();
    }


    /**
     * 群发列表
     */
    public function jpush(){

        return $this->fetch();
    }

    public function sendByAll(){

        if (IS_AJAX)
        {

            $redis = get_redis();

            $push_text = input('post.push_text') ?? '';
            if(!$push_text){
                $this->error('发送内容不能为空');
            }

            $sendByAll = [
                'send_type' => 'sendByAll',
                'send_data' => [
                    'alert'=>$push_text
                ],
            ];

            $redis->lPush($this->jpush_key,json_encode($sendByAll));
            // 插入发送数据
            $insertAll = [];
            $allUser = db('user')->all();
            $userIds = array_column($allUser,'id');
            foreach ($userIds as $key => $id){

                $tmp = [
                    'title' => '系统通知',
                    'content' => $push_text,
                    'send_uid' => 0,
                    'accept_uid' =>$id,
                    'sent_time' => date('Y-m-d H:i:s'),
                ];
                $insertAll[] = $tmp;
            }


            Db::connect([])->table('msg_record')->insertAll($insertAll);
            $this->success('发送成功');
        }
    }
    public function sendNotification(){

        if (IS_AJAX)
        {

            $push_text = input('post.push_text') ?? '';
            $gid = input('post.gid') ?? '';
            if(!$gid){
                $this->error('请输入商品ID');
            }
            if(!$push_text){
                $this->error('发送内容不能为空');
            }
            $pusher = getJpush();
            $pusher->setPlatform('all');
            $pusher->addAllAudience();
            $pusher->addAndroidNotification(
                $push_text,
                "惠好物",
                null,
                [
                    'gid' => $gid
                ]
            );
            $pusher->addIosNotification(
                $push_text, null,null,null,"惠好物",
                [
                    'gid' => $gid
                ]
            );
            try {
                $pusher->send();
                $this->success('发送成功');
            } catch (\JPush\Exceptions\JPushException $e) {
                // try something else here
                print $e;
                $this->error($e->getMessage());
            }

        }
    }

}