<?php

namespace app\admin\controller\good;

use think\Controller;
use app\common\controller\Admin;

class Miandan extends Admin
{
    protected $model;
    public function initialize()
    {
        parent::initialize();
        $this->model = model('GoodsMiandan');
    }
    /**
     * 列表
     */
      
    public function index()
    {
        if ($this->request->isAjax())
        {
            $list = $this->model
            ->order('sort desc')
            ->paginate(input('limit',15));
            $list->append(['is_tmall_text']);
            //返回layui分页
            return json(layui_page($list));
        }
        return $this->fetch();
    }
    /**
     * 添加免单商品
     */
    public function add()
    {
        $id = intval(input('id'));
        if (IS_AJAX){
            $data = input('post.');
            if ($data['id']){
                $re = $this->model->isUpdate(true)->save($data);
            }else {
                //添加
                if ($this->model->get(['item_id'=>$data['item_id']])){
                    $this->error('商品已经存在');
                }
                $re = $this->model->save($data);
            }
            if ($re){
                $this->success('保存成功');
            }else {
                $this->error('保存失败');
            }
        }else {
            $info = $this->model->get($id);
            $this->assign('info',$info);
            
            return $this->fetch();
        }
        
    }
    
    /**
     * 采集商品信息
     */
    public function item_info()
    {
        if (IS_AJAX){
           $item_id = input('item_id');
           if (empty($item_id)){
               $this->error('请输入商品ID');
           }
           //获取产品高佣信息
           $options = [
               'pid' => config('site.tkcfg.tk_pid')
           ];
           $tk = new \ddj\Tk($options);
           $item_info = $tk->gaoyong($item_id);
           if ($item_info){
               $this->success($item_info);
           }
           $this->error('采集失败');
        }
    }
    /**
     * 删除
     */
    public function del($ids='')
    {
        $type = input('type');
        if ($type=='all'){
            $count = db('goods_miandan')->delete(true);
            if ($count){
                $this->success('删除成功');
            }else {
                $this->error('删除失败');
            }
        }else {
            if ($ids){
                $count = $this->model->where('id','in',$ids)->delete();
                if ($count){
                    $this->success('删除成功');
                }else {
                    $this->error('删除失败');
                }
            }else{
                $this->error('请选择您要操作的数据');
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
