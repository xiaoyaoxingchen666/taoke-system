<?php

namespace app\common\model;

use think\Model;

class Goods extends Model
{
    public function getSectionIdTextAttr($value,$data)
    {
        $text = get_section_names();
        return $text[$data['section_id']];
    }
    public function getIsTmallTextAttr($value,$data){
        $text = ['淘宝','天猫'];
        return $text[$data['is_tmall']];
    }
}
