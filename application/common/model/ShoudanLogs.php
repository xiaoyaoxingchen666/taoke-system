<?php

namespace app\common\model;

use think\Model;

class ShoudanLogs extends Model
{
    // 关闭自动写入update_time字段
    //protected $updateTime = false;
    public function getStatusTextAttr($value,$data)
    {
        $text = ['失效','正常'];
        return $text[$data['status']];
    }
    public function getLevalTextAttr($value,$data)
    {
        $status = config('site.user_leval');
        return $status[$data['leval']];
    }
    public function getTypeTextAttr($value,$data)
    {
        $status = ['首单','注册'];
        return $status[$data['type']];
    }
    
}
