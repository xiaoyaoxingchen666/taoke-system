/**
  扩展一个test模块
**/      
layui.define(['layer','form','table'],function(exports){ //提示：模块也可以依赖其它模块，如：layui.define('layer', callback);
	var $ = layui.jquery,
		form = layui.form,
		table = layui.table;
	var tool = function(){
		this.init();
	}
	//追加验证
	form.verify({
      //我们既支持上述函数式的方式，也支持下述数组的形式
      //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
      money: [
         /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/
        ,'请输入正确的数值'
      ] 
    }); 
	tool.prototype.init=function(){
		this.checkAll();
		this.del();
		this.ajaxPost();
	}
	//全选的实现
	tool.prototype.checkAll=function(){
		form.on('checkbox(allChoose)', function(data){
		    var child = $('.layui-table-fixed table').find('.ids');
		    child.each(function(index, item){
		      //console.log(item);
		      item.checked = data.elem.checked;
		    });
		    form.render('checkbox');
		});
		// $(document).on('click','table .layui-form-checkbox',function(){
		// 	var hasChecked = $(this).hasClass('layui-form-checked');
		// 	alert(hasChecked);
		// 	$(this).prev().attr('checked',hasChecked);
		// 	form.render();
		// })
	}
	//选中
	tool.prototype.setValue=function setValue(name, value) {
	    var first = name.substr(0, 1),
	        input, i = 0,
	        val;
	    if (value === "") return;
	    if ("#" === first || "." === first) {
	        input = $(name);
	    } else {
	        input = $("[name='" + name + "']");
	    }

	    if (input.eq(0).is(":radio")) { //单选按钮
	        input.filter("[value='" + value + "']").each(function() {
	            this.checked = true
	        });
	    } else if (input.eq(0).is(":checkbox")) { //复选框
	        if (!$.isArray(value)) {
	            val = new Array();
	            val[0] = value;
	        } else {
	            val = value;
	        }
	        for (i = 0, len = val.length; i < len; i++) {
	            input.filter("[value='" + val[i] + "']").each(function() {
	                this.checked = true
	            });
	        }
	    } else { //其他表单选项直接设置值
	        input.val(value);
	    }
	    form.render(); //重新渲染
	}
	//单项删除
	tool.prototype.del=function(){
		$(document).on('click','.confirm_del',function(){
			var that = this;
			layer.confirm('确认要执行删除操作吗？', {
			  //btn: ['重要','奇葩'] //按钮
			  title:'提示'
			}, function(index){
			    $.get($(that).data('url'),function(ret){
			    	if (ret.code==1) {
			    		//针对有fixed的情况
						if ($(that).parents('.layui-table-box').find('.layui-table-fixed').length) {
							var tr_index = $(that).parents('tr').index();
							$('.layui-table-main tr').eq(tr_index).fadeOut();
							$('.layui-table-fixed').each(function(){
					    		$(this).find('tr').eq(tr_index+1).fadeOut();
					    	});
						}else{
							$(that).parents('tr').fadeOut();
						}
			    		
			    		layer.msg('操作成功',{icon:1});
			    	}else{
			    		layer.msg(ret.msg||'异常~',{icon:2});
			    	}
			    });
				layer.close(index);
			});	
		});

	}
	//用户信息
	tool.prototype.show_userinfo=function(){
		$(document).on('click','.show_userinfo',function(){
			var title = $(this).data('title')||'';
			var url = $(this).data('url');
			parent.layer.open({
		      title:title,
		      type: 2,
		      area: ['65%', '80%'],
		      fixed: false, //不固定
		      maxmin: true,
		      content: url,
		      shade:0
		      //id:$(this).data('lyid')||''
		    });
		});
		
	}
	//图片放大
	tool.prototype.show_img=function(){
		$(document).on('click','.show_img',function(){
			layer.open({
			  type: 1,
			  title: false,
			  closeBtn: 0,
			  //area: '516px',
			  skin: 'layui-layer-nobg', //没有背景色
			  shadeClose: true,
			  content: '<img src="'+$(this).attr('src')+'" height="100%" width="100%"/>'
			});
		});
		
	}
	//ajax-post
	tool.prototype.ajaxPost=function(){
		form.on('submit(ajax-post)', function(data){
			var target,query,form;
	        var target_form = $(this).attr('target-form');
	        var that = this;
	        var nead_confirm=false;
	        if( ($(this).attr('type')=='submit') || (target = $(this).attr('href')) || (target = $(this).attr('url')) ){
	            form = $('.'+target_form);
	            if ($(this).attr('hide-data') === 'true'){//无数据时也可以使用的功能
	            	if ( $(this).hasClass('confirm') ) {
	                    if(!confirm('确认要执行该操作吗...?')){
	                        return false;
	                    }
	                }
	            	form = $('.hide-data');
	            	query = form.serialize();
	            }else if (form.get(0)==undefined){
	            	return false;
	            }else if ( form.get(0).nodeName=='FORM' ){
	                if ( $(this).hasClass('confirm') ) {
	                    if(!confirm('确认要执行该操作吗.?')){
	                        return false;
	                    }
	                }
	                if($(this).attr('url') !== undefined){
	                	target = $(this).attr('url');
	                }else{
	                	target = form.get(0).action;
	                }
	                query = form.serialize();
	            }else if( form.get(0).nodeName=='INPUT' || form.get(0).nodeName=='SELECT' || form.get(0).nodeName=='TEXTAREA') {
	                form.each(function(k,v){
	                    if(v.type=='checkbox' && v.checked==true){
	                        nead_confirm = true;
	                    }
	                })
	                if ( nead_confirm && $(this).hasClass('confirm') ) {
	                    if(!confirm('确认要执行该操作吗..?')){
	                        return false;
	                    }
	                }
	                query = form.serialize();
	            }else{
	                if ( $(this).hasClass('confirm') ) {
	                    if(!confirm('确认要执行该操作吗...?')){
	                        return false;
	                    }
	                }
	                query = form.find('input,select,textarea').serialize();
	            }
	            $(that).addClass('disabled').attr('autocomplete','off').prop('disabled',true);
	            $.post(target,query).success(function(data){
					$(that).removeClass('disabled').prop('disabled',false);
	                if (data.code==1) {
						if($(that).hasClass('window_submit')){
							//对话框中的提交动作
							layer.msg(data.msg,{icon:1,time:1000},function(){
								//关闭弹窗并刷新父窗口
						        var frameid = $(window.parent.document).find('.layui-tab-item.layui-show iframe');
						        frameid.attr('src',frameid.attr('src'));
						        //当你在iframe页面关闭自身时
								var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
								parent.layer.close(index); //再执行关闭  
							});
							
						}else if($(that).hasClass('self_window')){
							layer.msg(data.msg,{icon:1,time:1000},function(){
								//关闭弹窗并刷新父窗口
								if(!$(that).hasClass('no_refresh')){
									 parent.location.reload();
								}
						        //当你在iframe页面关闭自身时
								var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
								parent.layer.close(index); //再执行关闭  
							});
						}else if($(that).hasClass('search')){
							$('#search').click();
							//layer.msg(data.msg)
						}
						else{
							if (data.url) {
								layer.msg(data.msg,{icon:1},function(){
									location.href=data.url;
								});
							}else{
								layer.msg(data.msg,{icon:1},function(){
									location.reload();
								});
							}
						}
						//成功后回调
						// var callback_func = $(that).data('callback');
						// if (callback_func) {
						// 	eval(callback_func+"('"+JSON.stringify(data)+"');")
						// }
	                }else{
						if($(that).hasClass('window_submit')){
							layer.msg(data.msg,{icon:2});
						}else{
							layer.msg(data.msg,{icon:2});
						}
	                }
	            },'json');
	        }
	        return false;
		});
	}


  	var tool = new tool();
  	//输出test接口
  	exports('tool', tool);
});   


