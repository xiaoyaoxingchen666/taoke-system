<?php /*a:2:{s:80:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/auth/adminuser/add.html";i:1546095382;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
</head>

<body>
	<div class="admin-body">
		
<form class="layui-form"> 
  <div class="layui-form-item">
    <label class="layui-form-label">所属角色：</label>
    <div class="layui-input-block">
      <select name="group" lay-filter="parent" lay-verify="required" multiple="multiple">
      <option value="">请选择...</option>
      <?php if(is_array($groupdata) || $groupdata instanceof \think\Collection || $groupdata instanceof \think\Paginator): $i = 0; $__LIST__ = $groupdata;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
        <option value="<?php echo htmlentities($key); ?>" <?php if(in_array($key,(array)$groupids)): ?>selected<?php endif; ?> ><?php echo htmlentities(html($vo)); ?></option>
      <?php endforeach; endif; else: echo "" ;endif; ?>
      
      </select>
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">用户名：</label>
    <div class="layui-input-inline">
      <input type="text" name="row[username]" value="<?php echo htmlentities($row['username']); ?>" placeholder="" autocomplete="off" class="layui-input" lay-verify="required">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">昵称：</label>
    <div class="layui-input-inline">
      <input type="text" name="row[nickname]" value="<?php echo htmlentities($row['nickname']); ?>" placeholder="" autocomplete="off" class="layui-input" lay-verify="required">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">密码：</label>
    <div class="layui-input-inline">
      <input type="password" name="row[password]" value="" placeholder="" autocomplete="off" class="layui-input" <?php if($config['actionname'] == 'add'): ?>lay-verify="required"<?php endif; ?>>
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">状态</label>
    <div class="layui-input-block">
      <input type="radio" name="row[status]" value="1" title="正常" checked>
      <input type="radio" name="row[status]" value="0" title="禁用" >
    </div>
  </div>
  
  <input type="hidden" name="" value="" />
  <div class="layui-form-item layui-sumbtn">
	  <div class="layui-input-block">
	    <button class="layui-btn window_submit" lay-submit="" lay-filter="ajax-post" type="submit" target-form="layui-form">立即提交</button>
	    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
	  </div>
  </div>
  
</form>

	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script type="text/javascript" src="/static/js/layui-mz-min.js"></script>
<script type="text/javascript">
layui.use(['tool'],function(){
	var $ = layui.jquery,form=layui.form,tool=layui.tool;
	
	<?php if($config['actionname'] == 'edit'): ?>
	tool.setValue('row[status]',<?php echo htmlentities($row->getData('status')); ?>);
	<?php endif; ?>
	layui.selMeltiple($);
});
</script>

</html>