<?php /*a:2:{s:74:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/article/cate.html";i:1546095382;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
</head>

<body>
	<div class="admin-body">
		
<blockquote class="layui-elem-quote">
	<button  class="layui-btn layui-btn-sm" id="add" data-url="<?php echo url('cate_add'); ?>">
		<i class="layui-icon">&#xe654;</i> 添加分类
	</button>
</blockquote>
<table class="layui-table">
  <thead>
    <tr>
      <th>分类名称</th>
      <th>修改时间</th>
      <th>操作</th>
    </tr> 
  </thead>
  <tbody>
  	<?php if(is_array($cate_list) || $cate_list instanceof \think\Collection || $cate_list instanceof \think\Paginator): $i = 0; $__LIST__ = $cate_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
    <tr>
      <td><?php echo htmlentities($vo['name']); ?></td>
      <td><?php echo htmlentities($vo['update_time']); ?></td>
      <td>
      	<div class="layui-btn-group">
		  <a class="layui-btn layui-btn-xs edit" data-url="<?php echo url('cate_edit',['id'=>$vo['id']]); ?>" >编辑</a>
		  <a class="layui-btn layui-btn-xs confirm_del" data-url="<?php echo url('cate_del',['id'=>$vo['id']]); ?>" >删除</a>
		</div>
      </td>
    </tr>
    <?php if(is_array($vo['childlist']) || $vo['childlist'] instanceof \think\Collection || $vo['childlist'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['childlist'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v2): $mod = ($i % 2 );++$i;?>
    <tr>
      <td>&nbsp;&nbsp;└<?php echo htmlentities($v2['name']); ?></td>
      <td><?php echo htmlentities($v2['update_time']); ?></td>
      <td>
      	<div class="layui-btn-group">
		  <a class="layui-btn layui-btn-xs edit" data-url="<?php echo url('cate_edit',['id'=>$v2['id']]); ?>" >编辑</a>
		  <a class="layui-btn layui-btn-xs confirm_del" data-url="<?php echo url('cate_del',['id'=>$v2['id']]); ?>" >删除</a>
		</div>
      </td>
    </tr>
    <?php endforeach; endif; else: echo "" ;endif; ?>
    <?php endforeach; endif; else: echo "" ;endif; ?>
  </tbody>
</table>

	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script>
layui.use(['tool'], function() {
	var $ = layui.$,layer = layui.layer, form = layui.form,table = layui.table,tool = layui.tool;
	//添加
	$(document).on('click','#add,.edit',function(){
	    var url = $(this).data('url'),title = '添加分类';
	    if($(this).hasClass('edit')){
	      url = $(this).data('url');
	      title = '编辑分类';
	    }
	  	layer.open({
	      title:title,
	      type: 2,
	      area: ['50%', '80%'],
	      fixed: false, //不固定
	      maxmin: true,
	      content: url,
	      shade:0,
	      id:'group_add'
	    });
	});
});
</script>

</html>